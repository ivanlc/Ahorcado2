/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.ControllerMainPlayView;
import Model.HumanPlayer;
import Model.MachinePlayer;
import Model.Player;
import java.awt.Color;
import java.awt.Font;
import java.awt.Label;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import jdk.nashorn.internal.ir.TryNode;

/**
 *
 * @author ivanl
 */
public class mainPlayView extends javax.swing.JFrame implements Observer {

    ControllerMainPlayView CtrlMainPlayerView = ControllerMainPlayView.getInstance();
    Label[] labHumanAux;
    Label[] labMachine1Aux;
    Label[] labMachine2Aux;
    Label[] labMachine3Aux;

    public mainPlayView() {
        initComponents();
    }

    public void addObserver(HumanPlayer pH, ArrayList listM) {
        Iterator i = listM.iterator();
        while (i.hasNext()) {
            MachinePlayer aux = (MachinePlayer) i.next();
            aux.addObserver(this);
        }

        pH.addObserver(this);
    }

    public void generarLettersHuman(char[] searchWord) {
        if (labHumanAux == null) {
            labHumanAux = new Label[searchWord.length];
        }

        for (int i = 0; i < searchWord.length; i++) {
            String letra = Character.toString(searchWord[i]);
            if (labHumanAux[i] == null) {
                labHumanAux[i] = new Label(letra);
            }

            labHumanAux[i].setFont(new Font("Tahoma", 0, 20));
            if ("_".equals(labHumanAux[i].getText())) {
                panelWord.add(labHumanAux[i], i);
            }

            labHumanAux[i].setText(letra);

        }

    }

    public void generarLettersMachine(MachinePlayer p) {
        char[] searchWord = p.getWordSearch().getWord();

        if (labMachine1Aux == null) {
            labMachine1Aux = new Label[searchWord.length];
        }

        if (labMachine2Aux == null) {
            labMachine2Aux = new Label[searchWord.length];
        }

        if (labMachine3Aux == null) {
            labMachine3Aux = new Label[searchWord.length];
        }

        if (p.getIdMachine() == 0) {

            for (int i = 0; i < searchWord.length; i++) {
                String letra = Character.toString(searchWord[i]);

                if (!letra.equals("_")) {
                    letra = "*";
                }

                if (!letra.equals("_")) {
                    letra = "*";
                }

                if (labMachine1Aux[i] == null) {
                    labMachine1Aux[i] = new Label(letra);
                }

                labMachine1Aux[i].setFont(new Font("Tahoma", 0, 20));
                if ("_".equals(labMachine1Aux[i].getText())) {
                    panelWordMachine1.add(labMachine1Aux[i], i);
                }

                labMachine1Aux[i].setText(letra);

            }
        }

        if (p.getIdMachine() == 1) {
            for (int i = 0; i < searchWord.length; i++) {
                String letra = Character.toString(searchWord[i]);

                if (!letra.equals("_")) {
                    letra = "*";
                }

                if (labMachine2Aux[i] == null) {
                    labMachine2Aux[i] = new Label(letra);
                }

                labMachine2Aux[i].setFont(new Font("Tahoma", 0, 20));
                if ("_".equals(labMachine2Aux[i].getText())) {
                    panelWordMachine2.add(labMachine2Aux[i], i);
                }

                labMachine2Aux[i].setText(letra);

            }
        }

        if (p.getIdMachine() == 2) {
            for (int i = 0; i < searchWord.length; i++) {
                String letra = Character.toString(searchWord[i]);

                if (!letra.equals("_")) {
                    letra = "*";
                }
                if (labMachine3Aux[i] == null) {
                    labMachine3Aux[i] = new Label(letra);
                }

                labMachine3Aux[i].setFont(new Font("Tahoma", 0, 20));
                if ("_".equals(labMachine3Aux[i].getText())) {
                    panelWordMachine3.add(labMachine3Aux[i], i);
                }

                labMachine3Aux[i].setText(letra);

            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMachine1 = new javax.swing.JPanel();
        panelWordMachine1 = new javax.swing.JPanel();
        lblMachine1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblVidas1 = new javax.swing.JLabel();
        panelMachine2 = new javax.swing.JPanel();
        panelWordMachine2 = new javax.swing.JPanel();
        lblMachine2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblVidas2 = new javax.swing.JLabel();
        panelMachine3 = new javax.swing.JPanel();
        panelWordMachine3 = new javax.swing.JPanel();
        lblMachine3 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblVidas3 = new javax.swing.JLabel();
        panelHuman = new javax.swing.JPanel();
        panelWord = new javax.swing.JPanel();
        lblPalabra = new javax.swing.JLabel();
        labelVida = new javax.swing.JLabel();
        lblVidasH = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        buttonA = new java.awt.Button();
        buttonB = new java.awt.Button();
        buttonC = new java.awt.Button();
        buttonD = new java.awt.Button();
        buttonE = new java.awt.Button();
        buttonF = new java.awt.Button();
        buttonG = new java.awt.Button();
        buttonH = new java.awt.Button();
        buttonI = new java.awt.Button();
        buttonJ = new java.awt.Button();
        buttonK = new java.awt.Button();
        buttonL = new java.awt.Button();
        buttonM = new java.awt.Button();
        buttonN = new java.awt.Button();
        buttonO = new java.awt.Button();
        buttonP = new java.awt.Button();
        buttonQ = new java.awt.Button();
        buttonR = new java.awt.Button();
        buttonS = new java.awt.Button();
        buttonU = new java.awt.Button();
        buttonW = new java.awt.Button();
        buttonT = new java.awt.Button();
        button23 = new java.awt.Button();
        buttonV = new java.awt.Button();
        buttonX = new java.awt.Button();
        buttonY = new java.awt.Button();
        buttonZ = new java.awt.Button();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 255));
        setName("FramePlayGame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(960, 427));

        panelMachine1.setBackground(new java.awt.Color(51, 255, 204));

        lblMachine1.setText("Skynet");

        jLabel1.setText("Vidas: ");

        lblVidas1.setText("0");

        javax.swing.GroupLayout panelMachine1Layout = new javax.swing.GroupLayout(panelMachine1);
        panelMachine1.setLayout(panelMachine1Layout);
        panelMachine1Layout.setHorizontalGroup(
            panelMachine1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMachine1Layout.createSequentialGroup()
                .addGroup(panelMachine1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMachine1Layout.createSequentialGroup()
                        .addGroup(panelMachine1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelMachine1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblVidas1))
                            .addGroup(panelMachine1Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(panelWordMachine1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 42, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMachine1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblMachine1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelMachine1Layout.setVerticalGroup(
            panelMachine1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMachine1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMachine1)
                .addGap(53, 53, 53)
                .addComponent(panelWordMachine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                .addGroup(panelMachine1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblVidas1))
                .addContainerGap())
        );

        panelWordMachine1.getAccessibleContext().setAccessibleParent(panelWordMachine1);

        panelMachine2.setBackground(new java.awt.Color(51, 255, 204));

        lblMachine2.setText("Majimbu");

        jLabel2.setText("Vidas: ");

        lblVidas2.setText("0");

        javax.swing.GroupLayout panelMachine2Layout = new javax.swing.GroupLayout(panelMachine2);
        panelMachine2.setLayout(panelMachine2Layout);
        panelMachine2Layout.setHorizontalGroup(
            panelMachine2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMachine2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblVidas2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMachine2Layout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addComponent(panelWordMachine2, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMachine2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblMachine2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelMachine2Layout.setVerticalGroup(
            panelMachine2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMachine2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMachine2)
                .addGap(49, 49, 49)
                .addComponent(panelWordMachine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMachine2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVidas2)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        panelMachine3.setBackground(new java.awt.Color(51, 255, 204));

        lblMachine3.setText("ET");

        jLabel3.setText("Vidas: ");

        lblVidas3.setText("0");

        javax.swing.GroupLayout panelMachine3Layout = new javax.swing.GroupLayout(panelMachine3);
        panelMachine3.setLayout(panelMachine3Layout);
        panelMachine3Layout.setHorizontalGroup(
            panelMachine3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMachine3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMachine3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMachine3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblVidas3))
                    .addGroup(panelMachine3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(panelWordMachine3, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(57, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMachine3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMachine3, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelMachine3Layout.setVerticalGroup(
            panelMachine3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMachine3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMachine3)
                .addGap(52, 52, 52)
                .addComponent(panelWordMachine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMachine3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblVidas3))
                .addContainerGap())
        );

        lblPalabra.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblPalabra.setText("Palabra");

        labelVida.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelVida.setText("Vidas:");

        lblVidasH.setText("0");

        javax.swing.GroupLayout panelHumanLayout = new javax.swing.GroupLayout(panelHuman);
        panelHuman.setLayout(panelHumanLayout);
        panelHumanLayout.setHorizontalGroup(
            panelHumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHumanLayout.createSequentialGroup()
                .addGroup(panelHumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelHumanLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(labelVida)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblVidasH))
                    .addGroup(panelHumanLayout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(lblPalabra))
                    .addComponent(panelWord, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelHumanLayout.setVerticalGroup(
            panelHumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHumanLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(lblPalabra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelWord, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(panelHumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelVida)
                    .addComponent(lblVidasH))
                .addContainerGap())
        );

        panelWord.getAccessibleContext().setAccessibleParent(panelWord);

        buttonA.setLabel("A");
        buttonA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAActionPerformed(evt);
            }
        });

        buttonB.setLabel("B");
        buttonB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBActionPerformed(evt);
            }
        });

        buttonC.setLabel("C");
        buttonC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCActionPerformed(evt);
            }
        });

        buttonD.setLabel("D");
        buttonD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDActionPerformed(evt);
            }
        });

        buttonE.setLabel("E");
        buttonE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEActionPerformed(evt);
            }
        });

        buttonF.setLabel("F");
        buttonF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonFActionPerformed(evt);
            }
        });

        buttonG.setLabel("G");
        buttonG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGActionPerformed(evt);
            }
        });

        buttonH.setLabel("H");
        buttonH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonHActionPerformed(evt);
            }
        });

        buttonI.setLabel("I");
        buttonI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonIActionPerformed(evt);
            }
        });

        buttonJ.setLabel("J");
        buttonJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonJActionPerformed(evt);
            }
        });

        buttonK.setLabel("K");
        buttonK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonKActionPerformed(evt);
            }
        });

        buttonL.setLabel("L");
        buttonL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLActionPerformed(evt);
            }
        });

        buttonM.setLabel("M");
        buttonM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMActionPerformed(evt);
            }
        });

        buttonN.setLabel("N");
        buttonN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNActionPerformed(evt);
            }
        });

        buttonO.setLabel("O");
        buttonO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOActionPerformed(evt);
            }
        });

        buttonP.setLabel("P");
        buttonP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPActionPerformed(evt);
            }
        });

        buttonQ.setLabel("Q");
        buttonQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonQActionPerformed(evt);
            }
        });

        buttonR.setLabel("R");
        buttonR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRActionPerformed(evt);
            }
        });

        buttonS.setLabel("S");
        buttonS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSActionPerformed(evt);
            }
        });

        buttonU.setLabel("U");
        buttonU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUActionPerformed(evt);
            }
        });

        buttonW.setLabel("W");
        buttonW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonWActionPerformed(evt);
            }
        });

        buttonT.setLabel("T");
        buttonT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTActionPerformed(evt);
            }
        });

        button23.setLabel("button1");

        buttonV.setLabel("V");
        buttonV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonVActionPerformed(evt);
            }
        });

        buttonX.setLabel("X");
        buttonX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonXActionPerformed(evt);
            }
        });

        buttonY.setLabel("Y");
        buttonY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonYActionPerformed(evt);
            }
        });

        buttonZ.setLabel("Z");
        buttonZ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonZActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(buttonA, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonB, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonC, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonD, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonE, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonF, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonG, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonH, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(buttonI, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonJ, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonK, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonL, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonM, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonN, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonO, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonP, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(buttonQ, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonR, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonS, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonT, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonU, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(button23, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonV, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonW, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonX, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(buttonY, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonZ, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(buttonQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(button23, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(buttonX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonY, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonZ, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelHuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelMachine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelMachine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelMachine3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 87, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelMachine2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelMachine1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelMachine3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(panelHuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTActionPerformed
        CtrlMainPlayerView.playHumanLetter('T');
        buttonT.setEnabled(false);
    }//GEN-LAST:event_buttonTActionPerformed

    private void buttonUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUActionPerformed
        CtrlMainPlayerView.playHumanLetter('U');
        buttonU.setEnabled(false);
    }//GEN-LAST:event_buttonUActionPerformed

    private void buttonOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOActionPerformed
        CtrlMainPlayerView.playHumanLetter('O');
        buttonO.setEnabled(false);
    }//GEN-LAST:event_buttonOActionPerformed

    private void buttonIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonIActionPerformed
        CtrlMainPlayerView.playHumanLetter('I');
        buttonI.setEnabled(false);
    }//GEN-LAST:event_buttonIActionPerformed

    private void buttonAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAActionPerformed
        CtrlMainPlayerView.playHumanLetter('A');
        buttonA.setEnabled(false);
    }//GEN-LAST:event_buttonAActionPerformed

    private void buttonBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBActionPerformed
        CtrlMainPlayerView.playHumanLetter('B');
        buttonB.setEnabled(false);
    }//GEN-LAST:event_buttonBActionPerformed

    private void buttonCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCActionPerformed
        CtrlMainPlayerView.playHumanLetter('C');
        buttonC.setEnabled(false);
    }//GEN-LAST:event_buttonCActionPerformed

    private void buttonDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDActionPerformed
        CtrlMainPlayerView.playHumanLetter('D');
        buttonD.setEnabled(false);
    }//GEN-LAST:event_buttonDActionPerformed

    private void buttonEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEActionPerformed
        CtrlMainPlayerView.playHumanLetter('E');
        buttonE.setEnabled(false);
    }//GEN-LAST:event_buttonEActionPerformed

    private void buttonFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFActionPerformed
        CtrlMainPlayerView.playHumanLetter('F');
        buttonF.setEnabled(false);
    }//GEN-LAST:event_buttonFActionPerformed

    private void buttonGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGActionPerformed
        CtrlMainPlayerView.playHumanLetter('G');
        buttonG.setEnabled(false);
    }//GEN-LAST:event_buttonGActionPerformed

    private void buttonHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonHActionPerformed
        CtrlMainPlayerView.playHumanLetter('H');
        buttonH.setEnabled(false);
    }//GEN-LAST:event_buttonHActionPerformed

    private void buttonJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonJActionPerformed
        CtrlMainPlayerView.playHumanLetter('J');
        buttonJ.setEnabled(false);
    }//GEN-LAST:event_buttonJActionPerformed

    private void buttonKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonKActionPerformed
        CtrlMainPlayerView.playHumanLetter('K');
        buttonK.setEnabled(false);
    }//GEN-LAST:event_buttonKActionPerformed

    private void buttonLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLActionPerformed
        CtrlMainPlayerView.playHumanLetter('L');
        buttonL.setEnabled(false);
    }//GEN-LAST:event_buttonLActionPerformed

    private void buttonMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMActionPerformed
        CtrlMainPlayerView.playHumanLetter('M');
        buttonM.setEnabled(false);
    }//GEN-LAST:event_buttonMActionPerformed

    private void buttonNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNActionPerformed
        CtrlMainPlayerView.playHumanLetter('N');
        buttonN.setEnabled(false);
    }//GEN-LAST:event_buttonNActionPerformed

    private void buttonPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPActionPerformed
        CtrlMainPlayerView.playHumanLetter('P');
        buttonP.setEnabled(false);
    }//GEN-LAST:event_buttonPActionPerformed

    private void buttonQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonQActionPerformed
        CtrlMainPlayerView.playHumanLetter('Q');
        buttonQ.setEnabled(false);
    }//GEN-LAST:event_buttonQActionPerformed

    private void buttonRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRActionPerformed
        CtrlMainPlayerView.playHumanLetter('R');
        buttonR.setEnabled(false);
    }//GEN-LAST:event_buttonRActionPerformed

    private void buttonSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSActionPerformed
        CtrlMainPlayerView.playHumanLetter('S');
        buttonS.setEnabled(false);
    }//GEN-LAST:event_buttonSActionPerformed

    private void buttonVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonVActionPerformed
        CtrlMainPlayerView.playHumanLetter('V');
        buttonV.setEnabled(false);
    }//GEN-LAST:event_buttonVActionPerformed

    private void buttonWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonWActionPerformed
        CtrlMainPlayerView.playHumanLetter('W');
        buttonW.setEnabled(false);
    }//GEN-LAST:event_buttonWActionPerformed

    private void buttonXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonXActionPerformed
        CtrlMainPlayerView.playHumanLetter('X');
        buttonX.setEnabled(false);
    }//GEN-LAST:event_buttonXActionPerformed

    private void buttonYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonYActionPerformed
        CtrlMainPlayerView.playHumanLetter('Y');
        buttonY.setEnabled(false);
    }//GEN-LAST:event_buttonYActionPerformed

    private void buttonZActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonZActionPerformed
        CtrlMainPlayerView.playHumanLetter('Z');
        buttonZ.setEnabled(false);
    }//GEN-LAST:event_buttonZActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mainPlayView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mainPlayView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mainPlayView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mainPlayView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mainPlayView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button button23;
    private java.awt.Button buttonA;
    private java.awt.Button buttonB;
    private java.awt.Button buttonC;
    private java.awt.Button buttonD;
    private java.awt.Button buttonE;
    private java.awt.Button buttonF;
    private java.awt.Button buttonG;
    private java.awt.Button buttonH;
    private java.awt.Button buttonI;
    private java.awt.Button buttonJ;
    private java.awt.Button buttonK;
    private java.awt.Button buttonL;
    private java.awt.Button buttonM;
    private java.awt.Button buttonN;
    private java.awt.Button buttonO;
    private java.awt.Button buttonP;
    private java.awt.Button buttonQ;
    private java.awt.Button buttonR;
    private java.awt.Button buttonS;
    private java.awt.Button buttonT;
    private java.awt.Button buttonU;
    private java.awt.Button buttonV;
    private java.awt.Button buttonW;
    private java.awt.Button buttonX;
    private java.awt.Button buttonY;
    private java.awt.Button buttonZ;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel labelVida;
    private javax.swing.JLabel lblMachine1;
    private javax.swing.JLabel lblMachine2;
    private javax.swing.JLabel lblMachine3;
    private javax.swing.JLabel lblPalabra;
    private javax.swing.JLabel lblVidas1;
    private javax.swing.JLabel lblVidas2;
    private javax.swing.JLabel lblVidas3;
    private javax.swing.JLabel lblVidasH;
    private javax.swing.JPanel panelHuman;
    private javax.swing.JPanel panelMachine1;
    private javax.swing.JPanel panelMachine2;
    private javax.swing.JPanel panelMachine3;
    private javax.swing.JPanel panelWord;
    private javax.swing.JPanel panelWordMachine1;
    private javax.swing.JPanel panelWordMachine2;
    private javax.swing.JPanel panelWordMachine3;
    // End of variables declaration//GEN-END:variables

    @Override
    public void update(Observable o, Object o1) {

        Player p = (Player) o;

        if (o1 instanceof String) {
            String turno = (String) o1;

            if (turno.equals("INICIOH")) {
                lblVidasH.setText(String.valueOf(p.getLife()));
                                if(p.getLife()<1)
                    lblPalabra.setText(p.getName() + ", has perdido!!!");
            }
            else if (turno.equals("INICIOM1")) {
                lblVidas1.setText(String.valueOf(p.getLife()));
                                if(p.getLife()<1)
                    lblMachine1.setText(p.getName() + " ha perdido!");
            }
            else if (turno.equals("INICIOM2")) {
                lblVidas2.setText(String.valueOf(p.getLife()));
                                if(p.getLife()<1)
                    lblMachine2.setText(p.getName() + " ha perdido!");
            }
            else if (turno.equals("INICIOM3")) {
                lblVidas3.setText(String.valueOf(p.getLife()));
                                if(p.getLife()<1)
                    lblMachine3.setText(p.getName() + " ha perdido!");
            }

            if (turno.equals("TURNH")) {
                panelHuman.setBackground(Color.GREEN);
                panelMachine1.setBackground(Color.RED);
                panelMachine2.setBackground(Color.RED);
                panelMachine3.setBackground(Color.RED);

                lblVidasH.setText(String.valueOf(p.getLife()));
                if(p.getLife()<1)
                    lblPalabra.setText(p.getName() + ", has perdido!!!");

            } else if (turno.equals("TURNM1")) {
                panelHuman.setBackground(Color.RED);
                panelMachine1.setBackground(Color.GREEN);
                panelMachine2.setBackground(Color.RED);
                panelMachine3.setBackground(Color.RED);

                lblVidas1.setText(String.valueOf(p.getLife()));
                if(p.getLife()<1)
                    lblMachine1.setText(p.getName() + " ha perdido!");

            } else if (turno.equals("TURNM2")) {
                panelHuman.setBackground(Color.RED);
                panelMachine1.setBackground(Color.RED);
                panelMachine2.setBackground(Color.GREEN);
                panelMachine3.setBackground(Color.RED);

                lblVidas2.setText(String.valueOf(p.getLife()));
                if(p.getLife()<1)
                    lblMachine2.setText(p.getName() + " ha perdido!");
            } else if (turno.equals("TURNM3")) {
                panelHuman.setBackground(Color.RED);
                panelMachine1.setBackground(Color.RED);
                panelMachine2.setBackground(Color.RED);
                panelMachine3.setBackground(Color.GREEN);

                lblVidas3.setText(String.valueOf(p.getLife()));
                if(p.getLife()<1)
                    lblMachine3.setText(p.getName() + " ha perdido!");
            }
        } else {

            boolean win = false;

            if (p instanceof HumanPlayer) {
                generarLettersHuman(p.getWordSearch().getWord());

                if (o1 != null) {
                    win = (boolean) o1;
                }

                if (win) {
                    lblPalabra.setText(p.getName() + " HAS GANADOOO!!!");
                }
            }

            if (p instanceof MachinePlayer) {

                generarLettersMachine((MachinePlayer) p);

                if (o1 != null) {
                    win = (boolean) o1;
                }

                if (win) {
                    if(p.getName() == lblMachine1.getText())
                        lblMachine1.setText(p.getName() + " HA GANADOOO!!!");
                    else if(p.getName() == lblMachine2.getText())
                        lblMachine2.setText(p.getName() + " HA GANADOOO!!!");
                    else if(p.getName() == lblMachine3.getText())
                        lblMachine3.setText(p.getName() + " HA GANADOOO!!!");
                }
            }

        }
    }
}
