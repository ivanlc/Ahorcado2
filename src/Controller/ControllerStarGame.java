/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.HumanPlayer;
import Model.MachinePlayer;
import Model.Word;
import Model.WordTurn;
import View.StarGameView;

/**
 *
 * @author u6032515
 */
public class ControllerStarGame {
    
    public void starGame() {
        
        StarGameView star = new StarGameView();
        star.setVisible(true);
                
    }
    
    public void star(String nombre) {
        
        Word word = new Word("A");
        WordTurn turnWord = new WordTurn(word);
        
        HumanPlayer pjH = new HumanPlayer(turnWord, nombre);
        MachinePlayer pjM1 = new MachinePlayer(turnWord, "Skynet");
        MachinePlayer pjM2 = new MachinePlayer(turnWord, "Majimbu");
        MachinePlayer pjM3 = new MachinePlayer(turnWord, "ET");
        
        ControllerMainPlayView viewMain = ControllerMainPlayView.getInstance();
        
        
        viewMain.play(pjH,pjM1,pjM2,pjM3);
//          viewMain.play(pjH,pjM1);
    }
    
}
