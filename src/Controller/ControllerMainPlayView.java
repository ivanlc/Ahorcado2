/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Model.*;
import View.StarGameView;
import View.mainPlayView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observer;
import javafx.scene.input.KeyCode;
import javassist.bytecode.Descriptor;



/**
 *
 * @author ivanl
 */
public class ControllerMainPlayView {
     
    static ControllerMainPlayView instance;
    ArrayList<MachinePlayer> listMachine = new ArrayList<>();
    HumanPlayer pHuman;
    
    public static ControllerMainPlayView getInstance() {
        
        if(instance == null)
            instance = new ControllerMainPlayView();
        return instance;
    }

    
    public void play (Player ... players) {

        for (int i = 0; players.length > i ; i ++)
        {
            if (players[i] instanceof MachinePlayer)
            {
                listMachine.add((MachinePlayer) players[i]);
            }
            
            else if (players[i] instanceof HumanPlayer)
            {
                pHuman = (HumanPlayer) players[i];
            }
        }
        
        this.pHuman = pHuman;
        mainPlayView game = new mainPlayView();
        game.addObserver(pHuman,listMachine);
                
        StartMachines();
        
        pHuman.update();
        new Thread(pHuman).start();           
        game.setVisible(true);
    }
    
    public void playHumanLetter(char letter)
    {
        pHuman.setLetter(letter);
    }
    
    public void StartMachines() {
        
        Iterator i = listMachine.iterator();
        while (i.hasNext())
        {
            MachinePlayer aux = (MachinePlayer) i.next();
            aux.update();
            new Thread(aux).start();
        }
        
    }
    
}
