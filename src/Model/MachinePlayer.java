/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author u6032515
 */
public final class MachinePlayer extends Player {
    
    List<Character> abcLetter;
    private int idMachine;
    static int id;
    
    public MachinePlayer(WordTurn wordTurn, String nombre) {
        super(wordTurn, nombre);
        this.abcLetter = new ArrayList();
        abcGenerator();
        idMachine = id++;
    }
    
    @Override
    public void run() {
        
        setChanged();
        notifyObservers("INICIOM" + (idMachine + 1));
        
        while (!getoWordTurn().isWin() && getLife() > 0) {
            
            getoWordTurn().getTurn((idMachine + 1));
            if(getoWordTurn().isWin())
                break;
            setChanged();
            notifyObservers("TURNM" + (idMachine + 1));
            
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MachinePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            randomletter();
            setHit(proposeLetter());
            if (getLife() < 1) {
                getoWordTurn().addPlayerLoser(idMachine + 1);
            }
            setChanged();
            notifyObservers(getoWordTurn().isWin());
            setChanged();
            notifyObservers("INICIOM" + (idMachine + 1));
            
            getoWordTurn().returnTurn(isHit());
            
        }
        
        setChanged();
        notifyObservers(getoWordTurn().isWin());
        
    }
    
    public void abcGenerator() {
        
        for (int i = 0; i < 26; i++) {
            
            char letter = (char) ('A' + i);
            abcLetter.add(letter);
            
        }
    }
    
    public void randomletter() {
        int randomNum = 0 + (int) (Math.random() * abcLetter.size());
        setLetter(abcLetter.remove(randomNum));
    }

//    public boolean proposeLetter() {
//    setLetter(randomletter());
//    char letter = getLetter();
//    boolean hit = false;
//
//    for (int i = 0 ; getoWordTurn().getLengthWord() > i ; i++) 
//    {
//        if (letter == getoWordTurn().getLetterWord(i)) 
//        {
//            getWordSearch().setletter(i, letter);
//            getWordSearch().hit();
//            hit = true;
//
//        }
//
//        if (getWordSearch().getComplete() == getoWordTurn().getWord().getArrayWord().length)
//        {
//            getoWordTurn().setWin(true);
//            break;
//        }
//    }       
//
//    if (hit == true){
//        getUsedLetter().add(letter);
//        abcLetter.remove((Character) letter);
//          
//    }   
//
//    return hit;
//    }
    public int getIdMachine() {
        return idMachine;
    }
    
    public void setIdMachine(int idMachine) {
        this.idMachine = idMachine;
    }
}
