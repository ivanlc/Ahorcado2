/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author u6032515
 */
public class WordSearch {
    
    private char[] word;
    private int complete;
    
    public WordSearch(int length) {
        this.word = new char[length];
        this.complete = 0;
        for (int i = 0 ; i<length ; i++)
        {
            getWord()[i] = '_';
        }
    }    
    
    public char[] getWord() {
        return word;
    }

    public void setletter(int pos, char letter) {
        this.word[pos] = letter;
    }

    public int getComplete() {
        return complete;
    }

    public void setComplete(int complete) {
        this.complete = complete;
    }
    
    public void hit() {
        complete ++;
    }
}
