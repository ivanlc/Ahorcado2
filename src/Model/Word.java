/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author u6032515
 */
public class Word {

    private String word;
    private char[] arrayWord;
    
    public Word(String word) {
        this.word = word;
        arrayWord = word.toCharArray();          
    }
    
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public char[] getArrayWord() {
        return arrayWord;
    }

    public void setArrayWord(char[] arrayWord) {
        this.arrayWord = arrayWord;
    }
    
    
    
}
