/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author u6032515
 */
public class HumanPlayer extends Player {

    public HumanPlayer(WordTurn wordTurn, String nombre) {
        super(wordTurn, nombre);
    }

    @Override
    public void run() {

        setChanged();
        notifyObservers("INICIOH");

        while (!getoWordTurn().isWin() && getLife()>0) {

        

                getoWordTurn().getTurn(0);
                if(getoWordTurn().isWin())
                    break;
                setChanged();
                notifyObservers("TURNH");
                while (getLetter() == ' ') {
                    Thread.yield();
                }
                setPlaying(true);

                setHit(proposeLetter());
                setChanged();
                notifyObservers(getoWordTurn().isWin());
                setChanged();
                notifyObservers("INICIOH");
                if(getLife()<1){
                    getoWordTurn().addPlayerLoser(0);
                }
                    

                getoWordTurn().returnTurn(isHit());
            

        }
        
        
    }

//    public boolean proposeLetter() {
//        
//        char letter = getLetter();
//        boolean hit = false;
//        
//        for (int i = 0 ; getoWordTurn().getLengthWord() > i ; i++) 
//        {
//            if (letter == getoWordTurn().getLetterWord(i)) 
//            {
//                getWordSearch().setletter(i, letter);
//                getWordSearch().hit();
//                hit = true;
//            }
//            
//            if (getWordSearch().getComplete() == getoWordTurn().getWord().getArrayWord().length)
//            {
//                getoWordTurn().setWin(true);
//                break;
//            }
//        }       
//        
//        if (hit == true)             
//            getUsedLetter().add(letter);                      
//        
//        setLetter(' ');
//        return hit;
//    }
}
