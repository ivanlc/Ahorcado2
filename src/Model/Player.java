/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author u6032515
 */
public abstract class Player extends Observable implements Runnable {
    
    private String name;
    private WordTurn oWordTurn;
    private WordSearch wordSearch;
    private List<Character> usedLetter;
    private int life;
    private char letter = ' ';
    private boolean playing = false;
    private boolean hit = false;

    public Player(WordTurn wordTurn,String nombre) {
        this.oWordTurn = wordTurn;
        this.wordSearch = new WordSearch(getoWordTurn().getWord().getArrayWord().length);
        usedLetter = new ArrayList<>();
        this.name = nombre;
        this.life = 10;
    }
    
    public boolean proposeLetter() {
        
        char letter = getLetter();
        boolean hit = false;
        
        for (int i = 0 ; getoWordTurn().getLengthWord() > i ; i++) 
        {
            if (letter == getoWordTurn().getLetterWord(i)) 
            {
                getWordSearch().setletter(i, letter);
                getWordSearch().hit();
                hit = true;
            }
            
            if (getWordSearch().getComplete() == getoWordTurn().getWord().getArrayWord().length)
            {
                getoWordTurn().setWin(true);
                break;
            }
        }       
        
        if (hit == true)             
            getUsedLetter().add(letter);   
        else
            life--;
        
        setLetter(' ');
        return hit;
    
    }
    
    public void update () {
        
        setChanged();
        notifyObservers();
    }
    

    public WordTurn getoWordTurn() {
        return oWordTurn;
    }

    public void setoWordTurn(WordTurn oWordTurn) {
        this.oWordTurn = oWordTurn;
    }

    public WordSearch getWordSearch() {
        return wordSearch;
    }

    public void setWordSearch(WordSearch wordSearch) {
        this.wordSearch = wordSearch;
    }

    public List<Character> getUsedLetter() {
        return usedLetter;
    }

    public void setUsedLetter(List<Character> usedLetter) {
        this.usedLetter = usedLetter;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    } 
}
