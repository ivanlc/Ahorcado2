package Model;

import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WordTurn {

    private Word word;
    private boolean win;
    private boolean enable;
    private int nextTurn;
    Hashtable<Integer, Integer> playerLoser = new Hashtable<>();

    public WordTurn(Word word) {
        this.word = word;
        this.win = false;
        this.enable = true;
        nextTurn = 0;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public synchronized void getTurn(int idPlayer) {

        while (!enable || idPlayer != nextTurn) {
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(WordTurn.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        enable = false;

        if (isWin()) {
            Thread.interrupted();
        }
    }

    public synchronized void returnTurn(boolean hit) {
//        if (!hit) {
        nextTurn++;
//        }
        if (nextTurn == 4) {
            nextTurn = 0;
        }
        while (!playerLoser.isEmpty() && playerLoser.containsKey(nextTurn) && playerLoser.size()<4) {
            if (nextTurn == 4) {
                nextTurn = 0;
            } else {
                nextTurn++;
            }
        }

        enable = true;
        notifyAll();

    }
    
    public void addPlayerLoser(int idPlayerLoser)
    {
        if(!playerLoser.containsKey(idPlayerLoser))
            playerLoser.put(idPlayerLoser, idPlayerLoser);
    }

    public int getLengthWord() {

        return getWord().getArrayWord().length;
    }

    public char getLetterWord(int letter) {
        return getWord().getArrayWord()[letter];
    }

}
